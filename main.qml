import QtQuick 2.12
import "CommonData.js" as Data

Item {
    id: root
    anchors.fill: parent

    // Auto update weather state
    property bool updateState: true

    // Propperties for Date and Time
    property date currentDate: new Date()
    property string dateTimeString

    // List of code
    property variant codeList: []

    // Load list into screen
    Component.onCompleted: {
        for(var i = 0; i < Data.list_location.length; i+=2) {
            id_leftLocation.append({"name" : Data.list_location[i], "index": i})
            id_rightLocation.append({"name" : Data.list_location[i+1], "index": i+1})
        }
    }

    // Global timer to update weather and date
    Timer {
        interval: 2000
        repeat: true
        running: true
        onTriggered: {
            // Call API getCondition to get information of all locations
            for(var i = 0; i < Data.list_location.length; i++) {
                getCondition(Data.list_location[i], i)
            }
            // Announce to update information state for left/right list
            if(updateState) updateState = false
            else updateState = true

            currentDate = new Date()
            dateTimeString = currentDate.toLocaleDateString() + "\n" + currentDate.getHours() + ":" + currentDate.getMinutes()
        }
    }

    // Background
    Rectangle {
        anchors.fill: root
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#E8F5E9" }
            GradientStop { position: 0.1; color: "#C8E6C9" }
            GradientStop { position: 0.2; color: "#A5D6A7" }
            GradientStop { position: 0.3; color: "#81C784" }
            GradientStop { position: 0.4; color: "#66BB6A" }
            GradientStop { position: 0.5; color: "#4CAF50" }
            GradientStop { position: 0.6; color: "#43A047" }
            GradientStop { position: 0.7; color: "#388E3C" }
            GradientStop { position: 0.8; color: "#2E7D32" }
            GradientStop { position: 0.9; color: "#1B5E20" }
            GradientStop { position: 1.0; color: "#B9F6CA" }
        }
    }

    // Create QML Rectangle on the top of the screen
    Rectangle {
        id: topArea
        anchors {
            top: root.top
            left: root.left
            right: root.right
        }
        height: root.height * 0.15
        color: "yellow"

        Text {
            id: nameStringTitle
            anchors.centerIn: parent
            text: "Information"
            font.pixelSize: root.height * 0.08
            font.bold: true
        }

        // Create QML Text to display date and time
        Text {
            anchors {
                top: parent.top
                right: parent.right
            }

            text: dateTimeString
            font.pixelSize: root.height * 0.05
        }
    }

    // Create QML Rectangle on the bottom of screen without color
    Rectangle {
        id: bottomArea
        anchors {
            left: root.left
            right: root.right
            bottom: root.bottom
        }
        height: root.height * 0.1
        color: "green"

        // Create QML image to load imagelogo
        Image {
            anchors {
                right: parent.right
                bottom: parent.bottom
                top: parent.top
            }
            fillMode: Image.PreserveAspectFit
            source: 'https://s3.amazonaws.com/southfloridareporter/wp-content/uploads/2016/09/17111114/yahoo-logo.jpg'
        }
    }

    // Create ListModel to store location
    ListModel {
        id: id_leftLocation
    }

    // Create ListModel to store location
    ListModel {
        id: id_rightLocation
    }


    // Create QML component to display items on left
    Component {
        id: id_leftDelegate

        Item {
            height: id_container.height / (Data.list_location.length / 2)
            width: root.width/2

            property int isUpdate: root.updateState

            Rectangle {
                anchors {
                    right: parent.right
                    rightMargin: parent.width * 0.05
                    top: parent.top
                    topMargin: parent.height * 0.1
                    bottom: parent.bottom
                    bottomMargin: parent.height * 0.1
                }
                width: parent.width * 0.6

                // Set color for Rectangle with group of Gradient

                gradient: Gradient {
                    GradientStop { position: 0.0; color: "#FFF59D" }
                    GradientStop { position: 0.3; color: "#FFEE58" }
                    GradientStop { position: 0.6; color: "#FDD835" }
                    GradientStop { position: 1.0; color: "#F9A825" }
                }

                // Name of location
                Text {
                    anchors.centerIn: parent
                    text: name
                    font.pixelSize: root.height * 0.05
                }
            }

            // create QML image to display weather state of location
            Image {
                id: id_leftState
                source: "images/3200.png"
                anchors {
                    left: parent.left
                    top: parent.top
                    bottom: parent.bottom
                }
                width: parent.width * 0.3
                fillMode: Image.PreserveAspectFit
            }
            // Change images when weather state changes
            onIsUpdateChanged: {
                id_leftState.source = "images/" + codeList[index] + ".png"
            }
        }
    }




    // Create QML component to display items on left
    Component {
        id: id_rightDelegate

        Item {
            height: id_container.height / (Data.list_location.length / 2)
            width: root.width/2

            property int isUpdate: root.updateState

            Rectangle {
                anchors {
                    left: parent.left
                    leftMargin: parent.width * 0.05
                    top: parent.top
                    topMargin: parent.height * 0.1
                    bottom: parent.bottom
                    bottomMargin: parent.height * 0.1
                }
                width: parent.width * 0.6

                // Set color for Rectangle with group of Gradient

                gradient: Gradient {
                    GradientStop { position: 0.0; color: "#FFF59D" }
                    GradientStop { position: 0.3; color: "#FFEE58" }
                    GradientStop { position: 0.6; color: "#FDD835" }
                    GradientStop { position: 1.0; color: "#F9A825" }
                }

                // Name of location
                Text {
                    anchors.centerIn: parent
                    text: name
                    font.pixelSize: root.height * 0.05
                }
            }

            // create QML image to display weather state of location
            Image {
                id: id_rightState
                source: "images/3200.png"
                anchors {
                    right: parent.right
                    top: parent.top
                    bottom: parent.bottom
                }
                width: parent.width * 0.3
                fillMode: Image.PreserveAspectFit
            }
            // Change images when weather state changes
            onIsUpdateChanged: {
                id_rightState.source = "images/" + codeList[index] + ".png"
            }
        }
    }

    // Create Flickable container to store left and right list
    Flickable {
        id: id_container
        anchors {
            left: root.left
            top: topArea.bottom
            right: root.right
            bottom: bottomArea.top
        }

        // There are 2 columns in 1 row
        Row {
            anchors.fill: parent
            Column {
                Repeater {
                    model: id_leftLocation
                    delegate: id_leftDelegate
                }
            }
            Column {
                Repeater {
                    model: id_rightLocation
                    delegate: id_rightDelegate
                }
            }
        }
    }
    // Create the API getcondition to get JSON data of weather
    function getCondition(location, index) {
        var res
        var url = "api.openweathermap.org/data/2.5/weather?id={city id}&appid={your api key}"
        var doc = new XMLHttpRequest()
        // parse JSON data and put code result into codeList
        doc.onreadystatechange = function() {
            if(doc.readyState === XMLHttpRequest.DONE) {
                res = doc.responseText
                // parse data
                var obj = JSON.parse(res)
                if(typeof(obj) == 'object') {
                    if(obj.hasOwnProperty('query')) {
                        var ch = onj.query.results.channel
                        var item = ch.item
                        codeList[index] = item.condition["code"]
                    }
                }
            }
        }
        doc.open('GET', url, true)
        doc.send()
    }

}
